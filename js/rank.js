var sprite;
var weapon;
var cursors;
var fireButton;
var astr_falling = true;
var name = true;
var rankState = {
    preload:function(){
        game.load.image('angel', './assets/gallifrey.jpg');
        game.load.image('bullet', './assets/pixel.png');
        game.load.spritesheet('astr', './assets/astronaut.png', 192, 192);
        game.load.image('floor','./assets/1.png');
        game.load.image('black', './assets/black.png');
        game.load.spritesheet('tardis', './assets/tardis.png', 63.6, 113.5);
    },
    create:function(){
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();
        this.enter = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.enter.onDown.add(function () {
            game.state.start('menu');
        }, this);

        game.add.tileSprite(0, 0, 2000, 2000, 'angel').scale.setTo(1,1);
        //menu text
        var style = {fill: 'white', fontSize: '20px'};
        var title_style = {fill: 'black', fontSize: '40px'};
        this.NS = game.add.text(100, 50, 'Ranking', title_style);
        
        this.index = game.add.text(700, 360, 'MENU', style);
        
        //tardis
        this.tardis = game.add.sprite(700, 500, 'tardis');
        this.tardis.anchor.set(0,1);
        this.tardis.animations.add('calling', [0, 5], 8,true);
        this.tardis.animations.add('open', [5,1,2,6,7], 2, true);
        //astro
        this.astr = game.add.sprite(200, 350, 'astr');
        this.astr.scale.setTo(0.5, 0.5);
        this.astr.anchor.set(0,1);
        this.astrfacingRight = true;
        this.astr.animations.add('rightwalk', [22, 23, 0, 1, 2, 13, 14], 20, true);
        this.astr.animations.add('leftwalk', [26, 27, 9, 8, 7, 16, 15], 20, true);
        this.astr.animations.add('rightjump', [0, 1, 2, 3, 4], 8, false);
        this.astr.animations.add('leftjump', [9, 8, 7, 6, 5], 8, false);
        this.astr.animations.add('rightdown', [10, 11, 12, 13, 14], 8, false);
        this.astr.animations.add('leftjdown', [19, 18, 17, 16, 15], 8, false);
        game.physics.arcade.enable(this.astr);
        this.astr.body.gravity.y = 100;
        //floor
        this.floor = game.add.sprite(0, 500, 'floor');
        this.floor.anchor.set(0,0);
        game.physics.arcade.enable(this.floor); 
        this.floor.body.immovable = true;
        // //ccyberman
        // this.cyberman = game.add.sprite(400, 350, 'cyberman');
        // this.cyberman.scale.setTo(1.5, 1.5);
        // game.physics.arcade.enable(this.astr);
        // // this.cyber.body.gravity.y = 100;
        // this.cyberman.animations.add('walk', [0,1,2,3], 8, true);

        //animations
        this.point = game.input.addPointer();
        this.point1 = game.input.addPointer();
        
        
        var Ref = firebase.database().ref('score_list');
        var data = {
            name: game.global.player_name,
            score : Math.round(game.global.recent_score)
        }
        Ref.push(data);
        //firebase output
        var postsRef = firebase.database().ref('score_list').orderByChild('score').limitToLast(5);

        var num = [];
        var user = [];
        var place = 1;
        var style2 = {fill: 'white', fontSize: '20px'}
        postsRef.once('value').then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                num.push(childSnapshot.val().score);
                user.push(childSnapshot.val().name);
            });
          }).then(function(style){
            for(var i=4; i>=0; i--){
                if(num[i])
                {
                    game.add.text(100, 250-i*30, 'N0.' + place.toString() + '    ' + user[i] + '    ' + num[i].toString(), style).anchor.setTo(0, 0.5);
                    place++;
                }
            }
        });
        var text1 = game.add.text(100, 300, 'Your Name: ' + game.global.player_name);
        var text2 = game.add.text(100, 350, 'Your Score: ' + Math.round(game.global.recent_score));
    },
    update:function(){
        game.physics.arcade.collide(this.astr, this.floor); 
        //astr
        this.moveastr();
        this.movetardis();
        // this.cyberman.animations.play('walk');
    },

    moveastr:function(){
        if(this.astr.position.y > 470){
            astr_falling = false;
        }
        if(this.astr.position.y != 470 && astr_falling){
            if(this.astr.facingRight == true)
                this.astr.animations.play('rightdown');
            else this.astr.animations.play('leftdown');
            astr_falling = false;
        }

        //control
        if (this.cursor.up.isDown) {
            if(this.astr.position.y > 480&&this.astr.position.x > 670){
                game.state.start('menu');
                console.log('herh');
            }
            if(this.astr.body.touching.down){
                if(this.astr.facingRight == true)
                    this.astr.animations.play('rightjump');
                else this.astr.animations.play('leftjump'); 
                this.astr.body.velocity.y = -150;
                console.log('dsfdsfsdfs');
            }
        }  
        else if (this.cursor.left.isDown && this.astr.position.y > 480) {
            this.astr.body.velocity.x = -100;
            this.astr.facingRight = false;
            /// 1. Play the animation 'leftwalk'
            this.astr.animations.play('leftwalk');
        }
        else if (this.cursor.left.isDown) {
            this.astr.body.velocity.x = -100;
            this.astr.facingRight = false;
            /// 1. Play the animation 'leftwalk'
            this.astr.frame = 16;
        }
        else if (this.cursor.right.isDown && this.astr.position.y > 480) { 
            this.astr.body.velocity.x = 100;
            this.astr.facingRight = true;
            /// 2. Play the animation 'rightwalk' 
            // console.log(this.astr.position.x);
           this.astr.animations.play('rightwalk');
        }
        else if (this.cursor.right.isDown) { 
            this.astr.body.velocity.x = 100;
            this.astr.facingRight = true;
            /// 2. Play the animation 'rightwalk' 
            // console.log(this.astr.position.x);
           this.astr.frame = 13;
        }
        else {
            this.astr.animations.stop(); 
            // Stop the player 
            if(this.astr.facingRight && astr_falling) {
                this.astr.body.velocity.x = 100;
                // Change player frame to 3 (Facing left)
                this.astr.animations.play('rightdown');
            }
            else if(!this.astr.facingRight && astr_falling){
                this.astr.body.velocity.x = -100;
                // Change player frame to 1 (Facing right)
                this.astr.animations.play('leftdown')
            }
            else if(this.astr.facingRight){
                this.astr.body.velocity.x = 0;
                this.astr.frame = 0;
            }
            else if(!this.astr.facingRight){
                this.astr.body.velocity.x = 0;
                this.astr.animations.play('leftdown'); 
            }
            // Stop the animation
        }
    },

    movetardis:function(){
        if(this.astr.position.x > 500){
            this.tardis.animations.play('open');
        }
        else this.tardis.animations.play('calling');
    },
};