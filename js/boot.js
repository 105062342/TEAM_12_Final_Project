var bootState = { 
    preload: function () {
        // Load the progress bar image.
        game.load.image('progressBar', 'assets/progressBar.png');
        game.load.audio('menu_music', 'assets/sounds/menu_music.wav');
        game.load.image('menu','./assets/menu.png');
    },
    create: function() {
        // Setting Menu state
        menu_musicSound = game.add.audio('menu_music');
        menu_musicSound.loop = true;
        menu_musicSound.volume = 0.3;

        game.stage.backgroundColor = '#3498db';
        game.state.start('load');
    } 
};