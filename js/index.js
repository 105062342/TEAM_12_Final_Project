var status = 'running';

var game = new Phaser.Game(800, 500, Phaser.AUTO, 'canvas');
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('main', mainState);
game.state.add('rank', rankState);
game.state.start('boot');

game.global ={
    player_name : undefined,
    recent_score: 0,
    rank_player : [],
    rank_score : []
}