var firstime = true;
var menuState = {
    preload:function(){},
    create:function(){
        // this.cursor = game.input.keyboard.createCursorKeys();
        game.add.tileSprite(0, 0, 2000,2000, 'menu').scale.setTo(0.5,0.5);

        //menu text
        var style = {fill: 'white', fontSize: '20px'};
        var title_style = {fill: 'white', fontSize: '40px'};
        this.NS = game.add.text(265, 80, 'GAME NAME', title_style);
        this.MENU = game.add.text(330, 150, 'MENU', title_style);
        this.index = game.add.text(270, 250, 'PLAY', style);
        this.index2 = game.add.text(430, 250, 'RANK', style);
        this.tri = game.add.sprite(245, 253 , 'tri');
        this.tri.anchor.set(0.5, 0.5);
        this.tri.scale.setTo(0.15,0.15);
        this.tri.visible = false;

        //player
        game.physics.startSystem(Phaser.Physics.ARCADE);
        // this.aircraft = game.add.sprite(265, 315, 'aircraft0');
        // this.player = game.add.sprite(300, 300, 'player');
        
        // game.physics.arcade.enable(this.player);
        // game.physics.arcade.enable(this.aircraft);
        //this.player.facingLeft = false;

        //animations
        this.point = game.input.addPointer();
        this.point1 = game.input.addPointer();
        //floor
        this.menu_floor = game.add.sprite(0, 500, 'menu_floor');
        this.menu_floor.anchor.set(0,0);
        game.physics.arcade.enable(this.menu_floor); 
        this.menu_floor.body.immovable = true;
        //cyberman
        this.cyberman = game.add.sprite(400, 400, 'cyberman');
        this.cyberman.scale.setTo(2.5, 2.5);
        this.cyberman.anchor.set(0.5,0.5);
        game.physics.arcade.enable(this.cyberman);
        this.cyberman.facingRight = true;
        this.cyberman.body.gravity.y = 100;
        this.cyberman.animations.add('walk', [0,1,2,3], 4, true);
        this.cyberman.animations.add('rightwalk', [8,9,10,11], 4, true);
        this.cyberman.animations.add('leftwalk', [4,5,6,7], 4, true);
        //bubble
        this.bubble1 = game.add.sprite(500, 330, 'bubble1');
        this.bubble1.anchor.set(0.5, 0.5);
        this.bubble1.scale.setTo(0.5, 0.5);
        this.bubble1.visible = true;
        firstime = true;
        this.bubble2 = game.add.sprite(200, 330, 'bubble2');
        game.physics.arcade.enable(this.bubble2);
        this.bubble2.anchor.set(0.5, 0.5);
        this.bubble2.scale.setTo(0.5, 0.5);
        this.bubble2.visible = false;
        this.bubble3 = game.add.sprite(550, 330, 'bubble3');
        game.physics.arcade.enable(this.bubble3);
        this.bubble3.anchor.set(0.5, 0.5);
        this.bubble3.scale.setTo(0.5, 0.5);
        this.bubble3.visible = false;
        
    },
    update: function(){
        game.physics.arcade.enable(this.tri, Phaser.Physics.ARCADE);
        game.physics.arcade.collide(this.cyberman, this.menu_floor);
        this.cursor = game.input.keyboard.createCursorKeys();
        this.menu();
        this.movecyberman();
        console.log(this.bubble2.body.position.x);

    },

    menu: function(){
        var keyboard2 = game.input.keyboard.addKeys({'enter': Phaser.Keyboard.ENTER});

        if(this.cursor.left.isDown && this.cyberman.body.position.x < 270){// || (game.input.mousePointer.x<350&&game.input.mousePointer.isDown)||(game.input.pointer1.x<350&&game.input.pointer1.isDown)) {
            this.tri.position.x = 245;
        }
        else if(this.cursor.right.isDown && this.cyberman.body.position.x > 390){//|| (game.input.mousePointer.x>=350&&game.input.mousePointer.isDown)||(game.input.pointer1.x>=350&&game.input.pointer1.isDown)){
            this.tri.position.x = 410;
        }
        if(keyboard2.enter.isDown){// || game.input.mousePointer.isDown || game.input.pointer1.isDown){
            if(this.tri.position.x == 245) {
                game.global.player_name = prompt("Please enter your name", "name");
                game.state.start('main');
            }
            if(this.tri.position.x == 410) {
                game.state.start('rank');
            } 
        }
    },
    movecyberman:function(){
        //bubble1
        if(this.cyberman.body.position.y == 380 &&this.cyberman.body.position.x == 360.625 && firstime){
            this.bubble1.visible = true;
        }
        else this.bubble1.visible = false;
        //bubble2
        if(this.cyberman.body.position.x < 270){
            this.bubble2.visible = true;
            this.bubble2.body.immovable = true;
        }
        else{
            this.bubble2.visible = false;
            this.bubble2.body.immovable = false;
        }
        if(this.bubble2.visible == true){
            if(this.cursor.left.isDown)
                this.bubble2.body.velocity.x = -150;
            else if(this.cursor.right.isDown)
                this.bubble2.body.velocity.x = 150;
            else this.bubble2.body.velocity.x = 0;
        }
        else this.bubble2.body.velocity.x = 0;
        //bubble3
        if(this.cyberman.body.position.x > 410 && this.bubble3.body.position.x >400) this.bubble3.visible = true;
        else this.bubble3.visible = false;
        if(this.bubble3.visible){
            if(this.cursor.left.isDown) 
                this.bubble3.body.velocity.x = -150;
            else if(this.cursor.right.isDown) 
                this.bubble3.body.velocity.x = 150;
            else this.bubble3.body.velocity.x = 0;
        }
        else this.bubble3.body.velocity.x = 0;
        //tri
        if(this.cyberman.body.position.x < 270||this.cyberman.body.position.x > 390) this.tri.visible = true;
        else this.tri.visible = false;

        //cyberman
        if(this.cyberman.body.position.y < 300) this.cyberman.frame = 0;
        else if(this.cursor.left.isDown){
            firstime = false;
            this.cyberman.body.velocity.x = -150;
            this.cyberman.facingRight = false;
            this.cyberman.animations.play('leftwalk');
        }
        else if(this.cursor.right.isDown){
            firstime = false;
            this.cyberman.body.velocity.x = 150;
            this.cyberman.facingRight = true;
            this.cyberman.animations.play('rightwalk');
        }
        else {
            this.cyberman.animations.stop();
            this.cyberman.body.velocity.x = 0;
            this.cyberman.frame = 0;
        }
    }
};